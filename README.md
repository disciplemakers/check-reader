# Check Reader API
API middleware for supported check readers.

## Start Server
```bash
$ cd server
$ sqlite3 server.db < server.db.sql
$ sudo python3 app.py
```

## Start Client
```bash
$ cd client

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## API Docs
Start the server.

[http://localhost:8000/docs](http://localhost:8000/docs)