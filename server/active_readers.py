from threading import Thread
import debugger
from check_reader_handlers import MagtekReaderHandler, RDMReaderHandler, CheckReaderHandler
from ftp_server import DummyFTP

class Singleton (type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class ActiveReaders(dict, metaclass=Singleton):

    handlers = {
        "magtek": MagtekReaderHandler,
        "rdm": RDMReaderHandler
    }

    def __init__(self, sleep_time = 5, handlers: dict = None) -> None:
        debugger.print("Initializing active readers handler")
        # self.experation_timer = Thread(target=self.checkExpired, daemon=True)
        self.sleep_time = sleep_time
        self.ftp = None
        if handlers:
            self.handlers = handlers
        self.started = False

    def newReaderHandler(self, reader_type) -> CheckReaderHandler:
        handler = self.handlers[reader_type](self)
        if reader_type == "magtek":
            if not self.ftp:
                self.ftp = DummyFTP()
                self.ftp_thread = Thread(target=self.ftp.start, daemon=True).start()
            handler.ftp = self.ftp
        return handler

    # def checkExpired(self):
    #     debugger.print("Starting expired and disconnected scanners check thread")
    #     while self.started:
    #         if len(self) > 0:
    #             remove = []
    #             for conn in self.values():
    #                 crh = conn["crh"]
    #                 user = conn["user_id"]
    #                 if not crh.connected:
    #                     debugger.print(f"Removing check reader {crh.host} from active readers")
    #                     remove.append(user)
    #                 elif not crh.active:
    #                     crh.disconnect()
    #                     debugger.print(f"Disconnecting check reader {crh.host} for {user}")
    #             for r in remove:
    #                 self.pop(r)
    #         time.sleep(self.sleep_time)



    def isReaderActive(self, reader_host):
        for crh in self.values():
            if reader_host == crh.host:
                return True
        return False

    def jsonSerialize(self):
        return {
            "active_readers": [{"conn_id": conn_id, **conn} for conn_id, conn in self.items()]
        }

    def start(self):
        if not self.started:
            self.started = True
            # self.experation_timer.start()

    def stop(self):
        self.started = False
        # self.experation_timer.join()