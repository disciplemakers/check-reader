#!/usr/bin/env python3
import os
from sqlite3 import connect

try:
    # Change working directory so relative paths (and template lookup) work again
    os.chdir(os.path.dirname(__file__))
except:
    pass
import toml

config = toml.load("config/config.toml")
if config["local"]:
    # Handle concurrent request when running locally
    from gevent import monkey

    monkey.patch_all()
from bottle.bottle import app, redirect, run, Bottle, static_file
from bottle.bottle_cors import EnableCors
from bottle.bottle_rest import API
from bottle.bottle_sqlite import SQLitePlugin
from bottle.bottle_jwt import JWTPlugin
from resources import Token, Connections, Check, Profiles, Users
import debugger
import requests
import argparse

debugger.debug = config["debug"] if "debug" in config else None
debugger.log_file = config["log_file"] if "log_file" in config else None

auth_config = toml.load("config/auth.toml")

app = Bottle()

# Setup plugins
api = API()
sqlite = SQLitePlugin("server.db")
jwt = JWTPlugin(auth_config["jwt_key"], Token.authenticate, debug=False)

# Install plugins
app.install(EnableCors())
app.install(sqlite)
app.install(jwt)
app.install(api)

# Add resources
api.addResource(Token, "/token")
api.addResource(Connections, "/connections", "/connections/<conn_id>")
api.addResource(Check, "/connections/<conn_id>/check")
api.addResource(Profiles, "/profiles", "/profiles/<name>")
api.addResource(Users, "/users", "/users/<user_id>")


@app.get("/docs")
def docs():
    return static_file("index.html", "../swagger-ui")


@app.get("/swagger<path:path>")
def swagger(path):
    return static_file(f"swagger{path}", "../swagger-ui")


@app.get("/docs/api.json")
def apiSpec():
    return static_file(f"api.json", "./")


@app.get("/")
def index():
    if config["local"]:
        try:
            return requests.get("http://localhost:3000/")
        except:
            redirect("/docs")


@app.get("/_nuxt<path:path>")
def nuxt(path):
    if config["local"]:
        try:
            return requests.get(f"http://localhost:3000/_nuxt{path}")
        except:
            redirect("/docs")


@app.get("/<path>")
def path(path):
    return index()


@app.get("/check-reader/app/_nuxt/<filename>")
def dashboardNuxt(filename):
    return static_file(filename, root="../client/dist/_nuxt")


@app.get(["/check-reader/app", "/check-reader/app<path:path>"])
def dashboard():
    print("Getting index")
    return static_file("index.html", root="../client/dist")


def clearConnections():
    print("Clearing connections table...\n")
    conn = connect("server.db")
    sql = "DELETE FROM connections WHERE 1"
    conn.execute(sql)
    conn.commit()
    conn.close()


def disconnectReaders():
    from active_readers import ActiveReaders

    readers = [r for r in ActiveReaders().values()]
    for reader in readers:
        crh = reader["crh"]
        print(f"Disconnecting {crh}")
        crh.disconnect()
        if crh.connected:
            print(f"Unable to disconnect from {reader}")


if __name__ == "__main__":
    # Check for DB
    if not os.path.isfile("server.db"):
        print("No database detected!")
        import admin_console

        admin_console.createDatabase()
    if not os.path.isfile("server.db"):
        print("\nPlease create a database before running the server!\n")
        exit()
    # Handle args
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", action="store_true")
    args = parser.parse_args()
    if args.c:
        clearConnections()
    # Run locally
    if config["local"]:
        print(f"Dashboard @ http://{config['host']}:{config['port']}/check-reader/app")
        run(app=app, host=config["host"], port=config["port"], server="gevent")
        disconnectReaders()
        clearConnections()
    # Run as WSGI
    else:
        application = app
