import builtins
from inspect import currentframe, stack
from datetime import datetime


class bcolors:
    HEADER = "\033[95m"
    BLUE = "\033[94m"
    CYAN = "\033[96m"
    GREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    NONE = "\u001b[37m"


debug = False
log_file = None


def setDebug(d: bool) -> None:
    global debug
    debug = d


def print(obj: object) -> None:
    cf = currentframe()
    if debug or log_file:
        file = stack()[1][1]
        timestamp = datetime.now().strftime("[%Y-%m-%d %H:%M:%S]")
        message = (
            f"{timestamp} <{file[file.rfind('/') + 1:]} {cf.f_back.f_lineno}> {obj}"
        )
        if debug:
            builtins.print(f"{bcolors.CYAN}{message}{bcolors.NONE}")
        if log_file:
            with open(log_file, "a+") as f:
                f.write(message + "\n")
