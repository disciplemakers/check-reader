import os
import hashlib
import sqlite3


def getUsers(db: sqlite3.Connection, username=None, password=False):
    sql = """SELECT users.id, users.username, users.permissions"""
    if password:
        sql += """, users.password"""
    sql += """ FROM users"""
    if username:
        sql += """ WHERE username=?"""
        query = db.execute(sql, (username,))
        return query.fetchone()
    else:
        query = db.execute(sql)
        return query.fetchall()


def authenticate(username, password, db):
    user = getUsers(db, username, password=True)
    if not user or isinstance(user, list):
        return False
    salty_key = bytes.fromhex(user["password"])
    salt = salty_key[:32]
    key = salty_key[32:]
    new_key = hashlib.pbkdf2_hmac("sha256", password.encode("utf-8"), salt, 100000)
    if new_key == key:
        return {
            "username": user["username"],
            "id": user["id"],
            "permissions": user["permissions"],
        }
    else:
        return False


def createUser(username, password, db, permissions="user"):
    salt = os.urandom(32)
    key = hashlib.pbkdf2_hmac("sha256", password.encode("utf-8"), salt, 100000)
    store = salt + key
    sql = """INSERT INTO users (username, password, permissions)
             VALUES (?, ?, ?)"""
    try:
        db.execute(sql, (username, store.hex(), permissions))
    except sqlite3.IntegrityError:
        return (False, {"message": "Username already exists."})
    return (True, {"message": f"User {username} created."})
