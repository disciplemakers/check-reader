import base64
from io import BytesIO
import logging
from PIL import Image
from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.servers import FTPServer
from pyftpdlib.handlers import FTPHandler
import debugger

class BytesLike(BytesIO):

    def __init__(self, filename: str = None, initial_bytes: bytes = bytes(), ) -> None:
        super().__init__(initial_bytes=initial_bytes)
        self.bytes = bytes()
        self.filename = filename[filename.rfind("/")+  1:]

    @property
    def name(self):
        return self

    def close(self) -> None:
        self.bytes = self.getvalue()
        return super().close()
    
    def __str__(self) -> str:
        return self.filename

class DummyFTP():

    def __init__(self):
        authorizer = DummyAuthorizer()
        authorizer.add_user('check-reader', '12345678', 'tmp', perm='elradfmwM')
        handler = FTPHandler
        handler.authorizer = authorizer
        handler.abstracted_fs.open = lambda self, filename, mode: BytesLike(filename)
        handler.on_file_received = self.on_file_received
        logging.basicConfig(filename='logs/pyftpd.log', level=logging.INFO)
        handler.permit_foreign_addresses = True
        address = ('0.0.0.0', 21)
        self.server = FTPServer(address, handler)
        self.server.max_cons = 256
        self.server.max_cons_per_ip = 5
        self.running = False
        self.images = {}

    def on_file_received(self, file: BytesLike):
        debugger.print("Received image via FTP")
        img = Image.open(BytesIO(file.bytes))
        png_bytes = BytesIO()
        img.save(png_bytes, "png")
        img = base64.b64encode(png_bytes.getvalue())
        img = img.decode("ascii")
        self.images[file.filename] = img
        debugger.print("Finished converting image")

    def removeImg(self, filename):
        self.images.pop(filename)

    def start(self):
        if not self.running:
            self.server.serve_forever()
            self.running = True

    def stop(self):
        self.images = {}
        if self.running:
            self.server.close_all()
            self.running = False
