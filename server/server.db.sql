BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS "profiles" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"name"	TEXT NOT NULL UNIQUE,
	"ftp_host"	TEXT,
	"host"	TEXT NOT NULL,
	"type"	INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS "reader_types" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"type"	TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"username"	TEXT UNIQUE,
	"permissions"	TEXT DEFAULT 'user',
	"password"	TEXT
);

CREATE TABLE IF NOT EXISTS "connections" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"reader_name"	TEXT NOT NULL UNIQUE,
	"user_id"	INTEGER NOT NULL,
	FOREIGN KEY("user_id") REFERENCES "users"("id") ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY("reader_name") REFERENCES "profiles"("name") ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO "reader_types" (id, type)
VALUES (1, "magtek");

INSERT INTO "reader_types" (id, type)
VALUES (2, "rdm");

COMMIT;
