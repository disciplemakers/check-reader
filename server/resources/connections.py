from bottle.bottle_rest import Resource
from bottle.bottle import request, response
from sqlite3 import IntegrityError, Connection
import requests
from active_readers import ActiveReaders
from .profiles import Profiles
import debugger


class Connections(Resource):
    GET = {"permissions": ["admin", "user"]}
    POST = {"permissions": ["admin", "user"]}

    def __init__(self) -> None:
        self.active_readers = ActiveReaders()
        self.active_readers.start()

    def getById(self, db: Connection, conn_id: int):
        debugger.print(f"Getting connection by id {conn_id} from database.")
        try:
            # Try to refresh check reader handler timeout
            crh = self.active_readers[conn_id]["crh"]
            crh.refreshTimeout()
        except:
            pass
        sql = "SELECT * FROM connections WHERE id = ?"
        return db.execute(sql, (conn_id,))

    def get(self, db: Connection, conn_id=None):
        debugger.print(f"Attempting to get all connections.")
        sql = "SELECT * FROM connections"
        return {"connections": db.execute(sql).fetchall()}

    def post(self, db: Connection, profile: str, timeout: int = 300):
        redacted = (
            "redacted" in self.params and self.params["redacted"].lower() == "true"
        )
        sql = "INSERT INTO connections (reader_name, user_id) VALUES (?, ?)"
        debugger.print(
            f"Attempting to create connection to check reader '{profile}' for user id {request.user['id']}."
        )
        try:
            # Create db entry for check reader connection
            debugger.print("Attempting to insert into connections table.")
            conn_id = db.execute(sql, (profile, request.user["id"])).lastrowid
            debugger.print(f"Inserted into connections table with id {conn_id}.")
        except IntegrityError as e:
            # Handle if check reader is already in the connections db table
            sql = "SELECT id, user_id FROM connections WHERE reader_name = ?"
            conn = db.execute(sql, (profile,)).fetchone()
            if conn and conn["user_id"] == request.user["id"]:
                # TODO handle id in db but not in active readers
                debugger.print(
                    f"Returning existing connection {conn['id']} for user id {conn['user_id']}."
                )
                return {"connected": True, "id": conn["id"]}
            else:
                debugger.print("Check reader already in use.")
                response.status = 503
                return {"messsage": "Check reader already in use."}
        except Exception as e:
            # Handle unknown errors
            debugger.print(f"Unexpected exception {e}.")
            response.status = 500
            return {"message": f"{type(e)} {e}"}
        profile = Profiles.getByName(db, profile)
        try:
            # Attempt to connent to the check reader
            debugger.print(
                f"Attempting to connect to check reader '{profile['name']}'."
            )
            crh = self.active_readers.newReaderHandler(profile["type"])
            crh.name = profile["name"]
            crh.connect(profile, conn_id, redacted=redacted, timeout_delay=int(timeout))
        except requests.exceptions.ConnectionError as e:
            # Handled check reader not found
            debugger.print(
                f"Unable to reach the cheack reader '{profile['name']}', rolling back the database."
            )
            db.rollback()
            response.status = 404
            return {"message": f"Unable to reach the cheack reader, is it turned on?"}
        except Exception as e:
            # Handle unknown error for connecting to check reader
            debugger.print(f"Unexpected exception {e}, rolling back the database.")
            db.rollback()
            response.status = 500
            return {"message": str(e)}
        if crh.connected:
            debugger.print(
                f"Successfully connected to check reader '{profile['name']}'."
            )
            response.status = 201
            response.add_header("location", f"/connections/{conn_id}")
            # Add check reader to in memory active readers if connection was successful
            self.active_readers[conn_id] = {"user_id": request.user["id"], "crh": crh}
            return {"connected": True, "id": conn_id}
        else:
            debugger.print(
                f"Unknown error. Likely a timeout or a silent http request failure."
            )
            db.rollback()
            response.status = 400

    @classmethod
    def deleteById(cls, db: Connection, conn_id: int):
        debugger.print(f"Attempting to delete connection id {conn_id} from database.")
        sql = "DELETE FROM connections WHERE id = ?"
        return db.execute(sql, (conn_id,)).lastrowid != None

    def delete(self, db: Connection, conn_id: int):
        conn_id = int(conn_id)
        debugger.print(f"Attempting to delete connection id {conn_id}.")
        if conn_id in self.active_readers:
            crh = self.active_readers[conn_id]["crh"]
            debugger.print(f"Attempting to disconnect from check reader '{crh.name}'.")
            crh.disconnect()
            if not crh.connected:
                # self.active_readers.pop(conn_id)
                if self.deleteById(db, conn_id):
                    debugger.print(f"Connection id {conn_id} deleted from database.")
                    return {"message": "Disconnected", "conn_id": conn_id}
                else:
                    # TODO handle failed to deleted from db
                    debugger.print(
                        f"Connection id {conn_id} not deleted from database."
                    )
                    pass
            else:
                debugger.print(f"Unable to disconnect from check reader '{crh.name}'.")
                response.status = 500
                return {"message": "Unable to disconnect"}
        else:
            # TODO handle connection not in actibe but in db
            debugger.print(f"Connection id {conn_id} not found")
            response.status = 404
            return {"message": "Connection not found"}

    def options(self):
        pass
