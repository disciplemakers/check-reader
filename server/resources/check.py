from sqlite3 import Connection
from bottle.bottle_rest import Resource
from bottle.bottle import request, response
from active_readers import ActiveReaders

class Check(Resource):

    GET = {
        "permissions": ["admin", "user"]
    }
    PUT = {
        "permissions": ["admin", "user"]
    }

    def __init__(self) -> None:
        self.active_readers = ActiveReaders()

    def handleRequest(self, conn_id):
        conn_id = int(conn_id)
        if conn_id in self.active_readers:
            if self.active_readers[conn_id]["user_id"] == request.user["id"]:
                crh = self.active_readers[conn_id]["crh"]
                if request.method == "GET":
                    return crh.getCheckData()
                elif request.method == "PUT":
                    return crh.ungetCheckData()
            else:
                response.status = 401
                return {"message": "You are not connected to this check reader"}
        else:
            response.status = 404
            return {"message": "Connection was not found"}

    def get(self, conn_id):
        return self.handleRequest(conn_id)

    def put(self, conn_id):
        return self.handleRequest(conn_id)

    def options(self):
        pass