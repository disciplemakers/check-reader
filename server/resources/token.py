from bottle.bottle_rest import Resource
from bottle.bottle import response
import hashlib

class Token(Resource):

    @classmethod
    def authenticate(cls, db, username, password):
        # user = getUsers(db, username, password=True)
        sql = """SELECT * FROM users WHERE username=?"""
        user = db.execute(sql, (username,)).fetchone()
        if not user:
            response.status = 401
            return {"message": "Authentication failed"}
        salty_key = bytes.fromhex(user["password"])
        salt = salty_key[:32]
        key = salty_key[32:]
        new_key = hashlib.pbkdf2_hmac("sha256", password.encode('utf-8'), salt, 100000)
        if new_key == key:
            return {
                "username": user["username"],
                "id": user["id"],
                "permissions": user["permissions"]
            }
        else:
            response.status = 401
            return {"message": "Authentication failed"}

    def post(self, db):
        pass

    def options(self):
        pass