from bottle.bottle_rest import Resource
from bottle.bottle import response
from sqlite3 import Connection, IntegrityError
import debugger

class Profiles(Resource):
    GET = {"permissions": ["admin", "user"]}
    POST = {"permissions": ["admin"]}
    PATCH = {"permissions": ["admin"]}
    DELETE = {"permissions": ["admin"]}

    @classmethod
    def getByName(cls, db, name):
        sql = """SELECT *, rt.type FROM profiles p, reader_types rt
                 WHERE p.type=rt.id AND p.name=?"""
        return db.execute(sql, (name,)).fetchone()

    def get(self, db: Connection, name=None):
        if name:
            return self.getByName(db, name)
        sql = "SELECT p.*, rt.type type_name FROM profiles p, reader_types rt WHERE p.type=rt.id"
        return {"profiles": db.execute(sql).fetchall()}

    def post(self, db: Connection, host, name, type, ftp_host=None):
        sql = """INSERT INTO profiles (name, host, ftp_host, type)
                 VALUES (?,?,?,?)"""
        try:
            prof_id = db.execute(sql, (name, host, ftp_host, type)).lastrowid
            response.status = 201
            response.add_header("location", f"/profiles/{prof_id}")
            return self.getByName(db, name)
        except IntegrityError:
            response.status = 409
            return {"message": "Profile already exists."}

    def patch(self, db: Connection, id, name=None, host=None, ftp_host=None, type=None):
        if name or host or ftp_host or type:
            sql = "UPDATE profiles SET "
            field_map = [
                ("name", name),
                ("host", host),
                ("ftp_host", ftp_host),
                ("type", type),
            ]
            updates = [f"{field}=?" for field, val in field_map if val]
            sql += ",".join(updates)
            sql += " WHERE id = ?"
            bindings = tuple(val for field, val in field_map if val)
            bindings += (id,)
            if db.execute(sql, bindings).rowcount > 0:
                return {"message": "Profile updated.", "profile_id": id}
            else:
                response.status = 404
                return {"message": "Profile not found."}
        else:
            response.status = 400
            return {"message": "Please supply a name, host, ftp_host, or type."}

    def deleteById(self, db: Connection, name: int):
        debugger.print(f"Attempting to delete check scanner {name} from database.")
        sql = "DELETE FROM profiles WHERE name = ?"
        return db.execute(sql, (name,)).lastrowid != None

    def delete(self, db: Connection, name: int):
        if self.deleteById(db, name):
            return {"message": "Profile deleted", "user_id": name}
        else:
            response.status = 404
            return {"message": "Profile not found"}


    def options(self):
        pass
