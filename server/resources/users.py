import hashlib
import os
from bottle.bottle_rest import Resource
from bottle.bottle import request, response
from sqlite3 import Connection, IntegrityError
import debugger


class Users(Resource):
    GET = {"permissions": ["admin", "user"]}
    POST = {"permissions": ["admin"]}
    PATCH = {"permissions": ["admin"]}
    DELETE = {"permissions": ["admin"]}

    def getById(self, db: Connection, user_id):
        sql = "SELECT id, username, permissions FROM users WHERE id = ?"
        return {"user": db.execute(sql, (user_id,)).fetchall()}

    def get(self, db: Connection, user_id=None):
        if user_id:
            if user_id == "current":
                return {"user": request.user}
            elif request.user["permissions"] == "admin":
                return self.getById(db, user_id)
        elif request.user["permissions"] == "admin":
            sql = "SELECT id, username, permissions FROM users"
            return {"users": db.execute(sql).fetchall()}

    def post(self, db: Connection, username, password, permissions="user"):
        salt = os.urandom(32)
        key = hashlib.pbkdf2_hmac("sha256", password.encode("utf-8"), salt, 100000)
        store = salt + key
        sql = """INSERT INTO users (username, password, permissions)
                VALUES (?, ?, ?)"""
        try:
            user_id = db.execute(sql, (username, store.hex(), permissions)).lastrowid
            response.status = 201
            response.add_header("location", f"/users/{user_id}")
            return self.getById(db, user_id)
        except IntegrityError:
            response.status = 409
            return {"message": "Profile already exists."}

    def patch(self, db: Connection, user_id, permissions="user"):
        sql = "UPDATE users SET permissions = ? WHERE id = ?"
        if db.execute(sql, (permissions, user_id)).rowcount > 0:
            return {"message": "User updated", "user_id": user_id}
        else:
            response.status = 404
            return {"message": "User not found"} 


    def deleteById(self, db: Connection, user_id: int):
        debugger.print(f"Attempting to delete user id {user_id} from database.")
        sql = "DELETE FROM users WHERE id = ?"
        return db.execute(sql, (user_id,)).lastrowid != None

    def delete(self, db: Connection, user_id: int):
        if self.deleteById(db, user_id):
            return {"message": "User deleted", "user_id": user_id}
        else:
            response.status = 404
            return {"message": "User not found"}

    def options(self):
        pass
