import sqlite3
import getpass
import auth
import secrets
import os
import subprocess
import jwt
import toml
import time
import sys


class bcolors:
    HEADER = "\033[95m"
    BLUE = "\033[94m"
    CYAN = "\033[96m"
    GREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"
    NONE = "\u001b[37m"


# Config
def createConfig():
    if input("Create config? (Y/n) ").lower() != "n":
        if not os.path.exists("config"):
            os.mkdir("config")
        with open("config/config.toml", "w+") as config:
            debug_mode = input("- Set debug mode? (y/N) ").lower() == "y"
            config.write(f"debug = {str(debug_mode).lower()}\n")
            print(
                f"{bcolors.GREEN}  └ Debugging is set {'on' if debug_mode else 'off'}.{bcolors.NONE}"
            )
            local_mode = input("- Run locally? (Y/n) ").lower() != "n"
            config.write(f"local = {str(local_mode).lower()}\n")
            port = input("- Enter port to run on (8000): ") or 8000
            config.write(f"port = {port}\n")
            config.write(f'host = "0.0.0.0"\n')
            ip_and_port = f"on 0.0.0.0:{port}"
            print(
                f"{bcolors.GREEN}  └ Check reader server will run {ip_and_port if local_mode else 'in wsgi mode'}.{bcolors.NONE}"
            )


def createJWTKey():
    if input("\nGenerate jwt key? (Y/n) ").lower() != "n":
        with open("config/auth.toml", "w+") as auth_file:
            jwt_key = secrets.token_urlsafe(16)
            auth_file.write(f'jwt_key = "{jwt_key}"')
            print(f"{bcolors.GREEN}- JWT key generated.{bcolors.NONE}")


def getAdminPassowrd():
    def valid_pass(password, match=None):
        if password == "":
            print(f"{bcolors.FAIL}  └ Please enter a password.\n{bcolors.NONE}")
            return False
        if match and match != password:
            print(
                f"{bcolors.FAIL}  └ Passwords do not match, try again.\n{bcolors.NONE}"
            )
            return False
        return True

    for i in range(3):
        admin_pass = getpass.getpass("- Enter admin password: ")
        if valid_pass(admin_pass):
            for j in range(3):
                match = getpass.getpass("- Enter password again: ")
                if valid_pass(admin_pass, match):
                    return admin_pass
            return None
    return None


def getAdminUsername(cursor):
    def valid_username(username):
        if auth.getUsers(cursor, admin_user):
            print(
                f"{bcolors.FAIL}  └ Username '{admin_user}'' already exists.\n{bcolors.NONE}"
            )
            return False
        elif username == "":
            print(f"{bcolors.FAIL}  └ Please enter a username.\n{bcolors.NONE}")
            return False
        else:
            return True

    for i in range(3):
        admin_user = input("- Enter admin username: ")
        if valid_username(admin_user):
            return admin_user
    return None


def createAdminUser():
    if input("\nCreate an admin user? (Y/n) ").lower() != "n":
        connection = sqlite3.connect("server.db")
        cursor = connection.cursor()
        admin_user = getAdminUsername(cursor)
        if admin_user:
            admin_pass = getAdminPassowrd()
            if admin_pass:
                auth.createUser(admin_user, admin_pass, cursor, "admin")
                print(
                    f'{bcolors.GREEN}  └ Admin user "{admin_user}" created.{bcolors.NONE}'
                )
                cursor.close()
                connection.commit()
                return True
        print(
            f"{bcolors.FAIL}  └ Too many failed attmpts, try again later.\n{bcolors.NONE}"
        )
        cursor.close()


# Create Database
def createDatabase():
    if input("\nBuild database? (Y/n) ").lower() != "n":
        connection = sqlite3.connect("server.db")
        cursor = connection.cursor()
        sql_file = open("server.db.sql")
        sql_as_string = sql_file.read()
        cursor.executescript(sql_as_string)
        print(f"{bcolors.GREEN}- Created database server.db\n{bcolors.NONE}")
        cursor.close()
        connection.commit()


def addReaderProfile():
    if input("\nCreate a check reader profile? (Y/n) ").lower() != "n":
        connection = sqlite3.connect("server.db")
        cursor = connection.cursor()
        sql = """INSERT INTO profiles (name, host, ftp_host, type)
                VALUES (?,?,?,?)"""
        values = [
            input(f"- Profile name (blank for 'default'): "),
            input(f"- Chech reader host address (ex. 192.168.1.1:443): "),
            input(f"- Ftp host address (likely same as the check reader server): "),
            input(
                f"- Type ({bcolors.CYAN}[1]{bcolors.NONE} Magtek | {bcolors.CYAN}[2]{bcolors.NONE} RDM): "
            ),
        ]
        if values[0] == "":
            values[0] = "default"
        try:
            cursor.execute(sql, tuple(values))
            print(f"{bcolors.GREEN}- Created profile {values[0]}{bcolors.NONE}")
        except sqlite3.IntegrityError:
            print(f"{bcolors.FAIL}- Profile already exists.{bcolors.NONE}")
        except Exception as e:
            print(f"{bcolors.FAIL}{e}{bcolors.NONE}")
        connection.commit()
        cursor.close()


def getToken():
    if input("\nGet bearer token? (Y/n) ").lower() != "n":
        jwt_key = toml.load("config/auth.toml")["jwt_key"]
        username = input("- Enter admin username: ")
        password = getpass.getpass("- Enter admin password: ")
        connection = sqlite3.connect("server.db")
        connection.row_factory = sqlite3.Row
        cursor = connection.cursor()
        user = auth.authenticate(username, password, cursor)
        if user:
            token = jwt.encode(user, jwt_key, algorithm="HS256")
            print(f"{bcolors.GREEN}- Token: {token}{bcolors.NONE}")
        else:
            print(f"{bcolors.FAIL}- Invalid username or password.{bcolors.NONE}")


def progressBar(count_value, total, suffix=""):
    bar_length = 25
    filled_up_Length = int(round(bar_length * count_value / float(total)))
    percentage = round(100.0 * count_value / float(total), 1)
    bar = "=" * filled_up_Length + "-" * (bar_length - filled_up_Length)
    print("[%s] %s%s ...%s" % (bar, percentage, "%", suffix), end="\r")


def startServer():
    args = ["python3", "app.py"]
    if input("\nClear connections table? (y/N) ").lower() == "y":
        args.append("-c")
    restart = input("Restart after manual shutdown? (y/N) ").lower() == "y"
    print()
    if restart:
        while restart:
            try:
                subprocess.call(args)
            except KeyboardInterrupt:
                print("\nRestarting server. Press Ctrl+C to cancel.\n")
                for i in range(11):
                    progressBar(i, 10)
                    time.sleep(0.2)
    else:
        subprocess.call(args)


def update():
    args = ["git", "pull"]
    print()
    subprocess.call(args)
    print("\nRestarting admin console.\n")
    os.execl(sys.executable, f"{sys.executable}", *sys.argv)


if __name__ == "__main__":
    alive = True

    def quit():
        global alive
        alive = False

    menu = {
        "Create config": createConfig,
        "Generate JWT Key": createJWTKey,
        "Create Database": createDatabase,
        "Add Admin User": createAdminUser,
        "Add Check Reader Profile": addReaderProfile,
        "Get Token": getToken,
        "Start Server": startServer,
        "Update": update,
        "Quit": quit,
    }
    while alive:
        print(
            f"What would you like to do? Press {bcolors.GREEN}[Enter]{bcolors.NONE} for first time setup."
        )
        for i, menu_item in enumerate(menu):
            print(f"{bcolors.CYAN}[{i+1}]{bcolors.NONE} {menu_item}")
        try:
            selection = input("> ")
        except KeyboardInterrupt:
            print(f"\n{bcolors.FAIL}Exiting{bcolors.NONE}\n")
            exit()
        except Exception:
            pass
        if selection.strip() == "":
            for x in menu.values():
                x()
        else:
            try:
                list(menu.values())[int(selection) - 1]()
            except sqlite3.OperationalError:
                print(
                    f"{bcolors.FAIL}  └ Please create database before creating and admin user.\n{bcolors.NONE}"
                )
            except IndexError:
                print(f"{bcolors.FAIL}- Invalid selection.\n{bcolors.NONE}")
            except KeyboardInterrupt as e:
                if list(menu.keys())[int(selection) - 1] == "Start Server":
                    print(f"{bcolors.FAIL}- Server terminated.\n{bcolors.NONE}")
                else:
                    print(f"\n{bcolors.FAIL}- Termiated.\n{bcolors.NONE}")
            except Exception as e:
                # print(type(e))
                # print(e.args)
                print(f"{bcolors.FAIL}- Invalid selection.\n{bcolors.NONE}")
    print(f"{bcolors.FAIL}Exiting{bcolors.NONE}\n")
