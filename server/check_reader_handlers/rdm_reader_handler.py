from .check_reader_handler import CheckReaderHandler
import base64
from io import BytesIO
import requests
import time
from xml.etree import ElementTree

# from threading import Thread
import urllib3
import urllib3.exceptions
from PIL import Image, ImageDraw, ImageFont
import debugger
from datetime import datetime
from urllib3.util import Retry
from requests.adapters import HTTPAdapter
from requests.models import Response

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class RDMSession(requests.Session):
    def __init__(self, max_retries=3, backoff_factor=0.1) -> None:
        super().__init__()
        retries = Retry(total=max_retries, backoff_factor=backoff_factor)
        self.mount("https://", HTTPAdapter(max_retries=retries))
        self.mount("http://", HTTPAdapter(max_retries=retries))

    def request(self, *args, **kwargs) -> Response:
        try:
            return super().request(*args, **kwargs)
        except Exception as e:
            print(type(e))
            res = requests.models.Response()
            res.status_code = 500
            res._content = f"{{'message': '{e}'}}".encode()
            return res


class RDMReaderHandler(CheckReaderHandler):
    def __init__(self, *args) -> None:
        super().__init__(*args)
        self.user_id = "DUMMY_USER"
        self.api = None
        self.session = RDMSession()

    def _connectToReader(self, profile_data):
        if "host" in profile_data:
            self.host = profile_data["host"]
        self.api = f"https://{self.host}/SCI/7.0/sci.esp"
        # disconnected = self._disconnectFromReader()
        # print(disconnected)
        res = self.session.post(
            self.api,
            verify=False,
            params={"Function": "ClaimScanner", "UserId": self.user_id},
            timeout=3.05
        )
        if res.status_code == 500:
            raise requests.exceptions.ConnectionError
        self.getResponse()
        self.connected = "Success" in str(res.content)
        if self.connected:
            self.startOperation()
        else:
            print(res.content)

    def _disconnectFromReader(self) -> bool:
        res = self.endOperation()
        if res.status_code == 500:
            return False
        res = self.session.post(
            self.api,
            verify=False,
            params={"Function": "ReleaseScanner", "UserId": self.user_id},
        )
        if "Success" in str(res.content):
            self.connected = False
            self.session.close()
            return True
        else:
            return False

    def startOperation(self):
        data = """<StartOperation><Micr><Enabled>true</Enabled><BeepOnError>true</BeepOnError><Parse>true</Parse>
                  </Micr><Frank><Enabled>false</Enabled></Frank><PhysicalEndorsement><Enabled>false</Enabled><Type>Bold
                  </Type><Data>Sequence:[SEQUENCE]</Data></PhysicalEndorsement><Image><Enabled>true</Enabled></Image>
                  </StartOperation>"""
        res = self.session.post(
            self.api,
            verify=False,
            params={
                "Function": "StartOperation",
                "UserId": self.user_id,
                "Parameter": 1,
                "Data": data,
            },
        )

    def getResponse(self):
        return self.session.post(
            self.api,
            verify=False,
            params={
                "Function": "GetResponseXml",
                "UserId": self.user_id,
                "Parameter": 0,
            },
        )

    def endOperation(self):
        return self.session.post(
            self.api,
            verify=False,
            params={
                "Function": "CompleteOperation",
                "UserId": self.user_id,
                "Parameter": 4,
            },
        )

    def _getMicrAndImg(self):
        res = self.getResponse()
        micr_and_img = None
        if res.content:
            if b"Timeout" not in res.content and b"Not Claimed" not in res.content:
                try:
                    tree = ElementTree.fromstring(res.content)
                    micr_and_img = self._getMicr(tree) + (self._getCheckImage(tree),)
                except Exception as e:
                    print(f"{type(e)} {e} in _getMicrAndImg()")
            else:
                # print(res.content)
                pass
            self.endOperation()
            self.startOperation()
            return micr_and_img

    def _getMicr(self, tree) -> tuple:
        try:
            parsed = tree.find("Micr").find("ParsedMicr")
            account = parsed.find("AccountNumber").text
            routing = parsed.find("TransitNumber").text
            check_no = parsed.find("CheckNumber").text
            return (routing, account, check_no)
        except AttributeError as e:
            return tuple(None for i in range(3))
        except Exception as e:
            print(f"{type(e)} {e} in _getMicr()")

    def _getCheckImage(self, tree) -> str:
        try:
            text_base64 = tree.find("ImageFront").find("Base64Data").text
            bytes_base64 = text_base64.encode()
            data = base64.b64decode(bytes_base64)
            img = Image.open(BytesIO(data))
            # Handle redaction
            if self.redacted:
                redaction = Image.new("RGB", (img.width - 20, 70), "Black")
                redaction_text = ImageDraw.Draw(redaction)
                font = ImageFont.truetype("arial.ttf", 58)
                try:
                    _, _, w, h = redaction_text.textbbox((0, 0), "Redacted", font=font)
                except:
                    w, h = (165, 58)
                redaction_text.text(
                    ((redaction.width - w) / 2, (redaction.height - h - 5) / 2),
                    "Redacted",
                    font=font,
                    fill=(255, 255, 255),
                )
                redaction.show()
                img.paste(redaction, (10, img.height - 85))
            png_bytes = BytesIO()
            img.save(png_bytes, "png")
            img = base64.b64encode(png_bytes.getvalue())
            img = img.decode("ascii")
            # print(f"Image: {img[0:30]}...")
            return img
        except Exception as e:
            print(f"{type(e)} {e} in _getCheckImage()")

    def _completeScan(self) -> None:
        pass

    def _completeListen(self) -> None:
        pass
        # self.endOperation()


if __name__ == "__main__":
    handler = RDMReaderHandler()
    handler.connect({"host": "192.168.0.205"})
    print(f"Connected: {handler.connected}")
    input("Press enter to quit.\n")
    handler.disconnect()
    print(f"Connected: {handler.connected}")
