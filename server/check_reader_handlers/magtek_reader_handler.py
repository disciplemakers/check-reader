from .check_reader_handler import CheckReaderHandler
import telnetlib
from threading import Thread
from datetime import datetime
import time
import os
from base64 import b64encode
import debugger

class MagtekReaderHandler(CheckReaderHandler):

    def __init__(self, *args):
        super().__init__(*args)
        self.scan_listener = Thread(target=self.listen, daemon=True)
        self.connected = False
        self.active = True
        self.check_data = []
        self.tel = None
        self.ftp = None

    def connect(self, profile_data, activity_timeout=300):
        self.profile_data = profile_data
        self.activity_timeout = activity_timeout
        self.profile_data["ftp_user"] = "check-reader"
        self.profile_data["ftp_pass"]= "12345678"
        if "host" in profile_data:
            self.host = profile_data["host"]
        else:
            pass  # TODO: Handle error
        if self.connected:
            self.disconnect()
        try:
            self.tel = telnetlib.Telnet(self.host, 23, timeout=1)
        except ConnectionRefusedError as e:
            debugger.print(e)
            return self.connected
        except Exception as e:
            debugger.print(e)
            return self.connected

        try:
            self.tel.read_until(b"Connection")
        except Exception as e:
            debugger.print(e)
            return self.connected

        # Attempt to setup the check reader, and return False if failed
        if not self.setupCheckReader():
            self.active = False
            return False

        self.connected = True
        self.scan_listener.start()
        return self.connected

    def setupCheckReader(self):
        self.tel.write(b"pr1=1\r\n")
        settings_map = {
            "ftp_host": "pr6",
            "ftp_user": "pr8",
            "ftp_pass": "pr10"
        }
        for ftp_setting, tel_setting in settings_map.items():
            if ftp_setting in self.profile_data:
                value = self.profile_data[ftp_setting]
                debugger.print(f"{tel_setting}={value}\r\n".encode())
                self.tel.write(f"{tel_setting}={value}\r\n".encode())
            else:
                pass  # TODO: Some error
        self.tel.write(b"pr7=1\r\n")
        self.tel.write(b"pr9=1\r\n")
        self.tel.write(b"pr11=1\r\n")
        self.tel.write(b"pr19=/\r\n")
        # self.tel.write(b"swi=10000000\r\n")
        self.tel.write(b"swi=00000000\r\n")
        return True

    def getMicr(self):
        try:
            micr = self.tel.read_until(b"\x03", 1).decode("ascii")
            # debugger.print(micr)
            routing = micr[micr.find("T") + 1:micr.rfind("T")]
            account = micr[micr.rfind("T") + 1:micr.rfind("U")]
            if micr[-4] == "1":
                number = int(micr[micr.find("U") + 1:micr.find("T") - 1])
            else:
                number = int(micr[micr.rfind("U") + 1:micr.find("/")])
            debugger.print("Reading check")
            debugger.print(f"check_no: {number}")
        except AttributeError:
            return None
        except IndexError:
            # No micr data found yet
            return None
        except ValueError:
            # Something other than micr data found
            return None
        except Exception as e:
            debugger.print(e)
        return (routing, account, number)

    def ftpUploadImage(self, time):
        timestamp = str(datetime.timestamp(time)).replace(".", "")
        debugger.print(f"timestamp: {timestamp}")
        debugger.print("Writing Image")
        self.tel.write(f"pr12={timestamp}\r\n".encode())
        self.tel.write(b"sa\r\n")
        self.tel.write(b"ti\r\n")
        tif = self.tel.read_until(f"{timestamp}".encode(), 10)
        if f"{timestamp}".encode() not in tif:
                debugger.print(f"FTP upload failed: {tif}")
                return False
        debugger.print("Image Uploaded")
        return timestamp

    def getCheckImage(self, timestamp):
        timeout = time.time() + 10
        while time.time() < timeout:
            debugger.print(timestamp)
            try:
                return self.ftp.images[timestamp]
            except:
                pass
            time.sleep(1)
        return False

    def removeImage(self, timestamp):
        self.ftp.removeImg(timestamp)
            
    def listen(self):
        debugger.print("Listening for checks")
        timeout = time.time() + self.activity_timeout
        while self.connected:
            try:
                routing, account, number = self.getMicr()
                timeout = time.time() + self.activity_timeout
            except:
                if time.time() > timeout:
                    debugger.print("Setting check reader inactive")
                    self.active = False
                continue
            now = datetime.now()
            timestamp = self.ftpUploadImage(now)
            if not timestamp:
                continue
            image_base64 = self.getCheckImage(timestamp)
            if not image_base64:
                continue
            self.check_data.append({
                "routing_number": "Test",  # routing,
                "account_number": "Test",  # account,
                "check_number": number,
                "image": image_base64,
                "image_content_type": "png",
                "scan_time": now.strftime("%Y-%m-%d %H:%M:%S"),
                "scan_id": timestamp
            })
            self.removeImage(timestamp)
            debugger.print("Scan complete")
        debugger.print("Listening ended")

    def disconnect(self):
        self.connected = False
        if self.tel:
            try:
                debugger.print("Closing telnet connection.")
                self.tel.write(b"quit\r\n")
                self.tel.close()
            except:
                pass
        self.scan_listener.join()
        self.scan_listener = Thread(target=self.listen, daemon=True)

    def getCheckData(self):
        timeout = time.time() + 60
        while time.time() < timeout:
            if len(self.check_data) > 0:
                return self.check_data.pop(0)
            time.sleep(1)
        return {}

    def clearCheckData(self):
        self.check_data = []
