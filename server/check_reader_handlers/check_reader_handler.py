from abc import ABC, abstractmethod
from threading import Thread
import time
from datetime import datetime
import debugger
import sqlite3

class CheckReaderHandler(ABC):

    def __init__(self, active_readers) -> None:
        self.scan_listener = Thread(target=self.listen, daemon=True)
        self.host = None
        self.name = None
        self.connected = False
        self.active = True
        self.check_data = []
        self.check_history = []
        self.activity_timeout = None
        self.timeout_delay = None
        self.active_reader = active_readers
        self.redacted = False
        super().__init__()

    @abstractmethod 
    def _connectToReader(self) -> None:
        pass

    def connect(self, profile_data, conn_id, redacted=False, timeout_delay=300) -> bool:
        self.profile_data = profile_data
        self.conn_id = conn_id
        self.timeout_delay = timeout_delay
        self.activity_timeout = time.time() + timeout_delay
        self._connectToReader(profile_data)
        if self.connected:
            # print("Connected")
            self.redacted = redacted
            self.scan_listener.start()
        return self.connected

    @abstractmethod 
    def _disconnectFromReader(self) -> None:
        pass

    def disconnect(self) -> bool:
        if not self.connected:
            return self.connected
        if self._disconnectFromReader():
            return self.active_reader.pop(self.conn_id, None) != None
        else:
            return False

    def _deleteConnFromDB(self):
        conn = sqlite3.connect("server.db")
        success = conn.execute("DELETE FROM connections WHERE id = ?", (self.conn_id,)).lastrowid != None
        conn.commit()
        conn.close()
        return success

    def listen(self) -> None:
        while self.connected:
            if time.time() > self.activity_timeout:
                debugger.print(f"Disconnecting from {self.profile_data['name']}")
                self.disconnect()
                if not self._deleteConnFromDB():
                    debugger.print(f"Failed to removed connection: {self.conn_id} from database")
                else:
                    debugger.print(f"Connection: {self.conn_id} removed from database")
                continue
            now = datetime.now()
            micr_and_img = self._getMicrAndImg()
            if micr_and_img:
                self.refreshTimeout()
                routing, account, number, image_base64 = micr_and_img
                self.check_data.append({
                    "routing_number": routing,
                    "account_number": account,
                    "check_number": number,
                    "image": image_base64,
                    "image_content_type": "png",
                    "scan_time": now.strftime("%Y-%m-%d %H:%M:%S"),
                    "scan_id": str(now.timestamp()).replace(".", "")
                })
                self._completeScan()
            time.sleep(0.1)
        self._completeListen()
        self.clearCheckData()

    @abstractmethod
    def _getMicrAndImg(self) -> tuple:
        return ()

    @abstractmethod
    def _completeScan(self) -> None:
        pass

    @abstractmethod
    def _completeListen(self) -> None:
        pass

    def getCheckData(self) -> dict:
        self.refreshTimeout()
        timeout = time.time() + 60
        while time.time() < timeout:
            if (not self.connected):
                return {"message": "Check reader no longer connected"}
            if len(self.check_data) > 0:
                check = self.check_data.pop(0)
                self.check_history.append(check)
                return check
            time.sleep(.25)
        return {"message": "Timed out"}

    def clearCheckData(self) -> None:
        self.check_data = []

    def ungetCheckData(self) -> None:
        self.refreshTimeout()
        if self.check_history:
            check = self.check_history.pop(len(self.check_history) - 1)
            self.check_data.insert(0, check)
            return {"message": "Check returned to beginning of the queue"}
        else:
            return {"message": "No checks in history"}

    def refreshTimeout(self):
        self.activity_timeout = time.time() + self.timeout_delay