export default function ({ store, redirect }) {
    return redirect(301, '/active_readers')
}