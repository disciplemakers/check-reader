export const state = () => ({
    check_reader: null,
    check_log: [],
    active_readers: [],
    connection_log: [],
    users: [],
    profiles: [],
    permissions: ["admin", "user"],
    reader_types: [{ text: "Magtek", value: 1 }, { text: "RDM", value: 2 }],
    connection_id: null,
    navs: [
        { text: "Token", to: "/token" },
        { text: "Active Readers", to: "/active_readers" },
        { text: "Check Reader Profiles", to: "/profiles" },
        { text: "Users", to: "/users" },
        { text: "Check Reader Test", to: "/checkreader" },
        { text: "Logout", to: "/login" },
    ]
});

export const mutations = {
    setCheckReader(state, check_reader) {
        state.check_reader = check_reader
    },
    addCheckData(state, check) {
        state.check_log.push(check)
    },
    setCheckLog(state, log) {
        state.check_log = log
    },
    addConnectionStatus(state, status) {
        state.connection_log.push(status)
    },
    setConnectionLog(state, log) {
        state.connection_log = log
    },
    setActiveReaders(state, active_readers) {
        if (active_readers.length > 0)
            state.active_readers = active_readers
        else
            state.active_readers = [{ "No active check readers": "" }]
    },
    setUsers(state, users) {
        state.users = users
    },
    setProfiles(state, profiles) {
        state.profiles = profiles
    },
    setPermissions(state, permissions) {
        state.permissions = permissions
    },
    setReaderTypes(state, reader_types) {
        state.reader_types = reader_types
    },
    setConnectionId(state, id) {
        state.connection_id = id
    }
}

export const actions = {
    async connectReader({ commit }, { profile = "default", redacted = false }) {
        const { data, status } = await this.$axios.post(`connections?redacted=${redacted}`, { profile: profile })
            .then((response) => response)
            .catch((error) => error.response)
        if (status == 201 || status == 200) {
            commit("setCheckReader", data)
            commit("setConnectionId", data.id)
        }
        commit("addConnectionStatus", data)
    },
    async disconnectReader({ commit, state }) {
        const { data } = await this.$axios.delete(`connections/${state.connection_id}`)
            .then((response) => response)
            .catch((error) => error.response)
        commit("setCheckReader", null)
        commit("addConnectionStatus", data)
    },
    async getScan({ commit, state }) {
        const { data, status } = await this.$axios.get(`connections/${state.connection_id}/check`)
            .then((response) => response)
            .catch((error) => error.response)
        if (status == 200)
            commit("addCheckData", data)
        else
            commit("setCheckReader", null)
    },
    async ungetScan({ commit, state }) {
        const { data, status } = await this.$axios.put(`connections/${state.connection_id}/check`)
            .then((response) => response)
            .catch((error) => error.response)
        if (status == 200)
            commit("addConnectionStatus", data)
        else
            commit("setCheckReader", null)
    },
    async getActiveReaders({ commit }) {
        const { data } = await this.$axios.get("/connections")
        commit("setActiveReaders", data.connections)
    },
    clearConnectionLog({ commit }) {
        commit("setConnectionLog", [])
    },
    clearCheckLog({ commit }) {
        commit("setCheckLog", [])
    },
    // Users
    async fetchUsers({ commit }) {
        const { data } = await this.$axios.get("users")
        commit("setUsers", data.users)
    },
    async createUser({ dispatch }, { username, password, permissions }) {
        const { data, error } = await this.$axios.post(`users`,
            { "username": username, "password": password, "permissions": permissions })
        dispatch("fetchUsers")
    },
    async updateUser({ dispatch }, { userId, permissions }) {
        const { data, error } = await this.$axios.patch(`users/${userId}`, { "permissions": permissions })
        dispatch("fetchUsers")
    },
    async deleteUser({ dispatch }, userId) {
        const { data, error } = await this.$axios.delete(`users/${userId}`)
        dispatch("fetchUsers")
    },
    // Profiles
    async fetchProfiles({ commit }) {
        const { data } = await this.$axios.get("profiles")
        commit("setProfiles", data.profiles)
    },
    async createProfile({ dispatch }, { name, host, ftp_host, type }) {
        const { data, error } = await this.$axios.post(`profiles`,
            { "name": name, "host": host, "ftp_host": ftp_host, "type": type })
        dispatch("fetchProfiles")
    },
    async updateProfile({ dispatch }, { id, name, host, ftp_host, type }) {
        const { data, error } = await this.$axios.patch(`profiles`,
            { "id": id, "name": name, "host": host, "ftp_host": ftp_host, "type": type })
        dispatch("fetchProfiles")
    },
    async deleteProfile({ dispatch }, name) {
        const { data, error } = await this.$axios.delete(`profiles/${name}`)
        dispatch("fetchProfiles")
    },
    // Permissions
    async fetchPermissions({ commit }) {
        const { data } = await this.$axios.get("permissions")
        commit("setPermissions", data.permissions)
    },
    async fetchReaderTypes({ commit }) {
        const { data } = await this.$axios.get("reader_types")
        commit("setReaderTypes", data.reader_types)
    },
    // Connections
    async deleteConnection({ dispatch }, connection_id) {
        const { data, error } = await this.$axios.delete(`connections/${connection_id}`)
        dispatch("getActiveReaders")
    },
}